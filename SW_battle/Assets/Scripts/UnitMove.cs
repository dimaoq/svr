﻿using UnityEngine;
using System.Collections;

public class UnitMove : MonoBehaviour {
    public Transform PosToGo;
    private float speed = 0.8f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 min = (PosToGo.position - transform.position) / (speed / Time.deltaTime);
        transform.Translate(min);
	}
}
