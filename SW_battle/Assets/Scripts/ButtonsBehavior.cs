﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonsBehavior : MonoBehaviour, IGvrGazeResponder
{
    public GameObject infoBubble;
    public Transform _camera;
    public Text text;
    public GameObject sphere;
    public string tet;
    public CameraController scr;
	// Use this for initialization
	void Start () {
        SetGazedAt(false);
        //text = infoBubble.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void LateUpdate () {
        //GvrViewer.Instance.UpdateState();
    }

    public void SetGazedAt(bool gazedAt)
    {
        if (gazedAt)
        {
            if (!infoBubble.activeInHierarchy)
            {
                infoBubble.SetActive(true);
            }

            text.text = tet;
            //Debug.Log(sphere.transform.localPosition.z);
            infoBubble.transform.localPosition = new Vector3(infoBubble.transform.localPosition.x, infoBubble.transform.localPosition.y, sphere.transform.localPosition.z);
            infoBubble.transform.LookAt(_camera.position);
            infoBubble.transform.Rotate(0.0f, 180.0f, 0.0f);
        }
        else
        {
            infoBubble.SetActive(false);
        }
    }

    public void BecomeDriver()
    {
        scr.SetUnfree();
    }

    public void ChangeShots()
    {
        scr.ChangeShot();
    }

    public void OnGazeEnter()
    {
        SetGazedAt(true);
    }

    /// Called when the user stops looking on the GameObject, after OnGazeEnter
    /// was already called.
    public void OnGazeExit()
    {
        SetGazedAt(false);
    }

    /// Called when the viewer's trigger is used, between OnGazeEnter and OnGazeExit.
    public void OnGazeTrigger()
    {
        //DoStuff();
        //TeleportRandomly();
    }
}
