﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour {

    private const float minPitchAngle = -60.0f;
    private const float maxPitchAngle = 60.0f;
    private const float SHOT_SPEED = 9000;
    private const float FIRE_RATE = 0.3f;
    

    private int lastInitInd;
    private int curAmmo;
    private bool isReloading;
    private float lastReloadingTime;
    private float nextShotTime;
    private bool isFreeMode;
    private System.Random rnd = new System.Random();

    public Transform cameraTransform;
    public Transform atObject;
    public GameObject[] laserInits = new GameObject[2];
    public Transform cabin;
    public Transform cabinCameraPos;
    public GameObject[] shots = new GameObject[2];
    private int curShotIndex = 0;
    public Text ammoText;
    public Text reloadingText;
    public Text autopilotText;
    public int maxAmmo = 60;
    public AudioSource audio;

    // Use this for initialization
    void Start () {
        lastInitInd = 0;
        curAmmo = maxAmmo;
        isReloading = false;
        lastReloadingTime = 0;
        nextShotTime = 0;
        ammoText.text = curAmmo + "/∞";

        //cameraTransform.transform.eulerAngles = new Vector3(0, 302, 0);
    }

    IEnumerator MoveCamera(bool back)
    {
        float mod = back ? -0.05f : 0.05f;
        for (float i = 1f; i <= 1.5f; i += Mathf.Abs(mod))
        {
            cameraTransform.position -= cabin.forward * mod;
            yield return null;
        }
    }

    void DoShoot()
    {
        var laserInit = laserInits[lastInitInd];
        lastInitInd = 1 - lastInitInd;
        GameObject shotObject = (GameObject)Instantiate(shots[curShotIndex], laserInit.transform.position, laserInit.transform.rotation);
        Rigidbody shotBody = shotObject.GetComponent<Rigidbody>();
        shotBody.AddForce(shotObject.transform.up * SHOT_SPEED);

        var audiop = laserInit.GetComponent<AudioSource>();
        audiop.Play();

        curAmmo--;
        if (curAmmo == 0)
        {
            isReloading = true;
            lastReloadingTime = Time.time;
            reloadingText.gameObject.SetActive(true);

        }

        ammoText.text = curAmmo + "/∞";
        nextShotTime = Time.time + FIRE_RATE;
    }

    public void SetUnfree()
    {
        StartCoroutine(MoveCamera(false));
        isFreeMode = false;
        autopilotText.gameObject.SetActive(false);
    }

    public void ChangeShot()
    {
        curShotIndex = 1 - curShotIndex;
    }
    // Update is called once per frame
    void Update() {
        if (isFreeMode)
        {
            if (!isReloading && Time.time >= nextShotTime && (rnd.Next(8) == 0))
            {
                DoShoot();
            }
            else
            {
                if (isReloading && (Time.time - lastReloadingTime > 2))
                {
                    reloadingText.gameObject.SetActive(false);
                    isReloading = false;
                    curAmmo = maxAmmo;
                    ammoText.text = curAmmo + "/∞";
                }
            }

            //if (rnd.Next(2) == 1)
            //{
            //    var angles = cabin.eulerAngles;
            //    int newF = rnd.Next(11) - 5;
            //    angles.y += newF;
            //    if (angles.y < 180) angles.y = 180;
            //    if (angles.y > 360) angles.y = 360;

            //    //if (angles.y >= 180 && angles.y <= 360)
            //    {
            //        //cabin.eulerAngles = angles;
            //        //cabin.position = cabinCameraPos.position;
            //    }
            //}

            if (rnd.Next(4) > 2)
            {
                var normalizedVector = new Vector3(cameraTransform.forward.x, 0, cameraTransform.forward.z);
                var d = normalizedVector * 0.25f * Input.GetAxisRaw("Vertical");
                cabin.position += d;
                cameraTransform.position += d;

                if (!audio.isPlaying)
                    audio.Play();
            }

            return;
        }

        if (!isFreeMode)
        {
            var angles = cabinCameraPos.eulerAngles;
            if (angles.y >= 75 && angles.y <= 270)
            {
                cabin.eulerAngles = angles;
                cabin.position = cabinCameraPos.position;
            }
            else
            {
                if (angles.y <= 30 && angles.y >= 0)
                {
                    isFreeMode = true;
                    autopilotText.gameObject.SetActive(true);
                    StartCoroutine(MoveCamera(true));
                    return;
                }
            }
        }

        if (!isReloading && Input.GetButtonDown("Fire1") && Time.time >= nextShotTime)
        {
            DoShoot();
        }
        else
        {
            if (isReloading && (Time.time - lastReloadingTime > 2))
            {
                reloadingText.gameObject.SetActive(false);
                isReloading = false;
                curAmmo = maxAmmo;
                ammoText.text = curAmmo + "/∞";
            }
        }

        if (!isFreeMode && Mathf.Abs(Input.GetAxisRaw("Vertical")) > 1e-3f)
        {
            var normalizedVector = new Vector3(cameraTransform.forward.x, 0, cameraTransform.forward.z);
            var d = normalizedVector * 0.25f * Input.GetAxisRaw("Vertical");
            cabin.position += d;
            cameraTransform.position += d;

            if (!audio.isPlaying)
                audio.Play();
        }
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 270)
            angle += 360;
        return Mathf.Clamp(angle, min, max);
    }
}
