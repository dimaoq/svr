﻿using UnityEngine;
using System.Collections;

public class TerrainBehavior : MonoBehaviour {

    public GameObject explosion;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Shot")
        {
            var exp = Instantiate(explosion, other.gameObject.transform.position, Quaternion.identity);
            DestroyObject(exp, 2f);
            DestroyObject(other.gameObject);
        }
    }
}
