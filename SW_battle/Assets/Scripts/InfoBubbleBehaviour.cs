﻿using UnityEngine;
using System.Collections;

public class InfoBubbleBehaviour : MonoBehaviour {

    public Transform _camera;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(_camera.position);
        transform.Rotate(0.0f, 180.0f, 0.0f);
	}
}
