﻿using UnityEngine;
using System.Collections;

public class FlightController : MonoBehaviour
{
    public Transform[] _waypoints;
    public float _speed;
    public float _rotationSpeed;
    public GameObject _ammo;
    public float _shotRate;
    public AudioSource audio;

    private float _lastShotTime;
    private int _waypointIndex;
    private CharacterController _character;

	void Start ()
    {
        _character = GetComponent<CharacterController>();
        _lastShotTime = Time.time;
	}
	
	void Update ()
    {
	    if (_waypointIndex >= _waypoints.Length)
        {
            _waypointIndex = 0;
        }
        FlightTo(_waypoints[_waypointIndex].position);
        if (Time.time - _lastShotTime > _shotRate)
        {
            _lastShotTime = Time.time;
            Shot();
        }
	}

    void FlightTo(Vector3 targetPos)
    {
        Vector3 moveDirection = targetPos - transform.position;

        if (moveDirection.magnitude < 0.5f)
        {
            _waypointIndex++;
        } 
        else
        {
            Quaternion rotation = Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotationSpeed);
            _character.Move(moveDirection.normalized * _speed * Time.deltaTime);
        }
    }

    void Shot()
    {
        GameObject shotObject = (GameObject)Instantiate(_ammo, transform.position, transform.rotation);
        shotObject.transform.Rotate(90, 0, 0);
        //shotObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        Rigidbody shotBody = shotObject.GetComponent<Rigidbody>();
        shotBody.AddForce(shotObject.transform.up * 9000);

        if (audio != null)
        audio.Play();
        Destroy(shotObject, 2.0f);
    }
}
